# cdb-rust
It's a clone with using rust and piston engine of my game [CrazyDancingBall](https://github.com/ryo33/CrazyDancingBall).  
![cdb-image](https://cloud.githubusercontent.com/assets/8780513/10998373/0cbd09a2-84d8-11e5-9373-1e2b3a9b12e1.png)

## Dependencies
- Freetype

## Play
```
$ cargo run
```
